# cdb

## License

Commercial. See [LICENSE](LICENSE.md).

## Install

See [Install](docs/technical/install.md) for getting set up.

## Contributors

See [Contributors](docs/CONTRIBUTORS.md).

## Getting started

First create a `config.mk` file with this content:

```text
cbl_config_file = config/dev/james.ini
all: echo "Using $(cbl_config_file)..."
```

Then run:

```bash
make
```

The first time you run this, a set of dependencies will be installed if you
haven't already put them all in the `deps` directory.

On subsequent runs, even if you delete the contents of the `build` and
`install` directories, the `make` program will be able to re-generate them from
`deps` without an internet connection.

## Run

```bash
make run
```

## Test

Run the server without an overridden `-etagPrefix` like this:

```bash
make run-test
```

Then start the tests like this:

```bash
make test
```

## Docs

```bash
godoc cmd/cbl/cmd
godoc -http=localhost:8073
```

## Pjax

Get these two files:

* `jquery-2.2.1.js`
* `jquery.pjax.js`

and put them in a `js` directory. Then run `make`
