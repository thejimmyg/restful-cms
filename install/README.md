# Install

This is where all the dependencies of everything in `src` get installed.

File and other builds take place in the `build` directory. Having a separate
folder for these two things keeps derived assets like these out of the `src`
tree.

The contents of this folder are not kept in source control, apart from:

* `README.md` (this file)
* `Makefile`
* `.npmrc` (tells `npm` where `../deps/npmcache` is

To run an install, run the command below in this directory:

```
cd ../deps
make
cd ../install
make
```

See [Build docs](../docs/technical/build.md) for more information.

## Run

```
CONFIG=../../../config/dev/james.ini make run
```
