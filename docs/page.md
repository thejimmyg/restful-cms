# Pages

Pages can be part of multiple sections, sections can have multiple locators.
This means:

* A page needs to list all relevant sections in its sidebar
* We just choose whichever cbl_locator has the highest weight

Page translations - each page can have a lang. We can choose to filter by lang
when looking in the `section_relation`.
