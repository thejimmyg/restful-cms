# Docs

For an overview of everything, see the [README](../README.md)

The documents listed here are the minimum we can get away with to effectively
specify the feature files that will drive the tests which will drive the
implementation.


## Feature Files

The feature files stored in `behaviour/browser/features`. See those files for
the detailed specification of the behaviour of the system.

Each feature file describes one user story and all the scenarios that story
has to deal with.

The feature files are executable and can be tested against target different
target deployments as described in [Testing](technical/test.md).


## Business documentation

* [Customer Segements](business/customer_segments.md)
* [Value Innovation](business/value_innovation.md)
* [Stategy Canvas](business/strategy_canvas.md)
* [Business Model](business/business_model.md)
* [Community Engagement](business/community.md)


## Technical Documentation

Design:

* [Architecture](technical/architecture.md)
* [Services, Responsibilities & Collaborators](technical/services.md)
* [Browsers](technical/browsers.md)
* [Entities](technical/entities.md)
* [Workflows](technical/workflows.md)
* [Wireframes](technical/wireframes.md)
* [Sequences](technical/sequences.md)

Policy and Process:

* [Install](technical/install.md)
* [Branching](technical/branching.md)
* [Build and Run](technical/build.md)
* [Release Candidates](technical/candidate.md)
* [Testing](technical/test.md)
* [Deploying](technical/deploy.md)

