# Test

Any code without tests is *legacy code* because you have no way to know how
changes you make to the code (or changes in code or services you depend on)
will affect the behavior of the system.

Sadly, many people are still writing new legacy code every day. This approach
does not scale beyond tiny projects.

## Feature Files

Tests are written as *feature files* in the `behaviour` directory:

```
cd behaviour
```

## Running

From the `behaviour` directory, make sure you have the translator running:

```bash
npm run test-behaviour:build-watch
```

Then run the tests from the same directory as you are running the server from.
The important thing to note is the `HOST` variable. This is what the tests will
be run against. Make sure this is set to the right setting for what you want to
test. You wouldn't want to:

* Run the tests against a working system instead of you current code, giving
  you a false sense that everything was fine
* Run against a production instance, accidentally making dangerous changes

Once you are sure the `HOST` variable is correct, run:

```bash
HOST=http://localhost:8071 npm run bdd-tests 2> test.log
```
