# Build

In this project we're describing *building* as the process of converting source
files into built files that are used by some other part of the project.

By contrast, a *candidate* is a single file containing everything that would
need to be released to production to get the server running or a new version
installed.

This document describes the process of building files. See
[candidate](candidate.md) for advice on creating a release candidate file.

## When

You will need to check you can build files any time you set up a pull request
to merge your feature into the `develop` branch.

The *release manager* will also need to build files as part of the process of
creating a candidate either *test* them or *release* them to `staging` or
`production`.

## To create a build you should simply be able to go into the `build` directory and run:

```
./build.sh
```

