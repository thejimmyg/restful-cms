# Deploy

To either:

* `test`
* `staging`
* `production`

Once you have your release candidate in the `candidate` directory, this should be easy:

```
cd candidate
./deploy test candidate_name
```
