# Models and Interfaces

These are best described using the built-in godoc-generated documentation. To
view them, run this:

```bash
godoc cmd/cbl/cmd
godoc -http=localhost:8073
```

Then visit http://localhost:8073/pkg/cbl
