# Services

The code currently relies only on one service:

* Webfaction

## Webfaction

This is a standard web host providing a CentOS 6.7 environment with a single
user account and PostgreSQL 9.1 database.
