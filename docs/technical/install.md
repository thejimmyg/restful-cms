# Install

## Database

Requires PostgreSQL 9.1 (or 9.5 if you want row level security).

Set up the database:

```bash
export CBLDB=james2 ; dropdb $CBLDB ; createdb $CBLDB ; psql -f sql/all.sql $CBLDB
```

## Go

```bash
export GOPATH=$PWD
go get github.com/lib/pq
go get bitbucket.org/ww/goautoneg
go get github.com/russross/blackfriday
go get github.com/microcosm-cc/bluemonday
go get github.com/vharitonsky/iniflags
go get github.com/NYTimes/gziphandler
go get github.com/matryer/silk
go install github.com/matryer/silk
```

## Run

```bash
# Note that the config argument must start ./ or /, and that arguments can be
# overridden with flags
go run src/cbl/cmd/main.go -dbName james2 -config ./default.ini -etagPrefix `date "+%s"`:
```

For testing, run the server without an overridden `-etagPrefix` like this:

```bash
go run src/cbl/cmd/main.go -dbName james2 -config ./default.ini
```
