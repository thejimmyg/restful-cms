# Candidate

Building a release candidate is about packaging up everything that needs to be
deployed on the target server into one file. This will include:

* Source code
* Built assets from the build directory
* Data
* Any dependencies

We don't have a process for this yet, but you can imagine it being similar to
the `build` step.

Once the file is produced, you can [deploy](deploy.md) it to the `test`
instance to run automated tests on, release it to `staging` for people to have
a play with, or release it to `production`.
