# Branching

We use the git-flow branching model for development.

First install the software by following the relevant guide:

* https://github.com/nvie/gitflow/wiki/Installation

Now read about it how to use git-flow:

* http://jeffkreeftmeijer.com/2010/why-arent-you-using-git-flow/

A description of all the command line options is here:

* https://github.com/nvie/gitflow/wiki/Command-Line-Arguments

You can run `git flow init` on an existing git repo or a new directory. Here
are the setting we've used on this project:

```
$ git flow init

Which branch should be used for bringing forth production releases?
   - master
Branch name for production releases: [master]
Branch name for "next release" development: [develop]

How to name your supporting branch prefixes?
Feature branches? [feature/]
Release branches? [release/]
Hotfix branches? [hotfix/]
Support branches? [support/]
Version tag prefix? [] v
```

Since this has already been done, you are ready to begin work.

All work is done in a *feature*. All features must be related to an *issue* in
the issue log. A feature can solve more than one issue, in which case you will
reference all the sub-issues in the main issue for the feature. You must name
your feature branch starting with the issue number of the issue and a `-`
character.

For example to start a new feature for branching strategy issue #12, you might
run this:

```
$ git flow feature start 12-branching-strategy
Switched to a new branch 'feature/12-branching-strategy'

Summary of actions:
- A new branch 'feature/12-branching-strategy' was created, based on 'develop'
- You are now on branch 'feature/12-branching-strategy'

Now, start committing on your feature. When done, use:

     git flow feature finish 12-branching-strategy
```

As the instructions say, you can now start commiting changes to your branch.
Please don't finish the feature when you are done though. Someone else will
finish your feature as part of a *pull request* (see later).

When you've made some changes you can *stage* new files (get them ready to commit):

```
git add <filename>
```

Or to *stage* changes to existing files you can run:

```
git add -p
```

This will go through each of the changes you've made and ask if you want to stage them. You type `y` for yes and `n` for no.

At the end you'll have a set of changes in the stage ready to commit. Commit them like this:

```
git commit -m "Description of the changes I've made"
```

When you are ready to publish your changes to create a *pull request* you can run this (you might need to enter your password a few times):

```
$ git flow feature publish 12-branching-strategy
Counting objects: 4, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 2.07 KiB | 0 bytes/s, done.
Total 4 (delta 1), reused 0 (delta 0)
remote:
remote: Create pull request for feature/12-branching-strategy:
remote:   https://.../pull-requests/new?source=feature/12-branching-strategy&t=1
remote:
To git@bitbucket.org:thejimmyg/webdb.git
 * [new branch]      feature/12-branching-strategy -> feature/12-branching-strategy
Already on 'feature/12-branching-strategy'
Your branch is up-to-date with 'origin/feature/12-branching-strategy'.

Summary of actions:
- A new remote branch 'feature/12-branching-strategy' was created
- The local branch 'feature/12-branching-strategy' was configured to track the remote branch
- You are now on branch 'feature/12-branching-strategy'
```

After the initial publish, you can update your published branch on the remote
repository like this:

```
$ git push origin feature/12-branching-strategy
```

Once you are happy, you should check that you can [build](build.md) a
*candidate* using your branch, run the [tests](test.md) and then create a pull
request (described next).

## Pull Requests

Once your feature is ready for review, please set up a pull request to merge it
into `develop` using the GitHub interface. Other people can discuss your
changes, check out your code and run the tests.

It is important that with no prior knowledge about what you've written and why,
a reviewer can work out everything they need to do to install and run the code
of your pull request. This means it is important that any migration
instructions or new setup that is required is documented as part of the code
and hightlighted in the pull request description so the reviewer can find it.

When they are happy they'll accept your pull request and your changes will be
merged to `develop`.

See also:

* [Build](build.md) - How to create a single deployable *candidate* from 

## Advanced

The one thing that isn't explained terribly well in the article you read to
start with is what happens if someone finishes their feature and gets it merged
into `develop` before you finish yours. Or if hotfixes or release branch merges
get into `develop`.

The answer is that you won't be able to merge your changes back until you get
the new version of `develop`. You can do this in two ways:

*rebase*
: This takes all the changes you've made so far, and re-plays them on top of the
  commits that have just been merged into `develop` as if all your changes
  happened *after* all the commits that have just arrived in `develop`. You are
  *re-writing history*, but only on your branch.

*merge*
: You can just take all the differences and apply them in one go to your branch
  as a single patch. Since all the new changes are now in your branch, you can
  carry on and know that you will be able to merge your changes back. No history
  re-writing required.

If your feature branch is purely local, you can rebase it on top of develop.
However, it takes time to understand how rebase works, and before you do, it's
quite easy to get yourself in quite a mess, and to mess up other people's work
too. Merge commits might look noisy but merging is guaranteed to always be safe
and predictable.

You can merge `develop` onto your current branch like this:

```
git merge develop
```

and then see the history like this:

```
git log --all --graph --oneline --decorate
```

If you find yourself needing to merge to often, it could be a sign that your
features are too big and too long-lived.

To see the differences between your current feature branch and `develop` you can run:

```
git flow feature diff
```

To switch to a different feature branch, you can run:

```
git flow feature checkout <name>
```
