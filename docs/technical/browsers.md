# Browser Standards

The Get Safe Online project recommends users always run the latest version of
their browser, on the latest version of their operating system with the latest
updates to both installed in order to be safe online.

* https://www.getsafeonline.org/protecting-your-computer/update-your-browser/

Not all internet users follow this advice though. The issues for us as web software developers are:

1. Older browsers have fewer features and can't easily be forced into doing
   many of the things people expect of modern web software

2. In trying to support older or non-standard browsers we might leave ourselves
   open to unexpected security risks

## Legacy Browser Support Cost

Issue 1. is mainly an issue of cost. It costs twice as much to support two versions of a site. If only a small number of your users will use that part of the site, is it worth the cost?

When choosing to support legacy browsers, a common approach is:

* Make sure old browsers can read the information
* Only allow modern browsers to change it

## Security

Issue 2. is mainly an issue of risk.

To mitigate the risk, here are some guidelines:

* Never support browsers that the browser manufacturer no longer supports, no-one is fixing the security problems that are discovered
* Don't rely on third party plugins or extensions for the functionality you need (the browser manufacturer won't support them so any problems will be your responsibility)
* Don't support browsers that a tiny number of people use (those people are either unlikely to use your service anyway, or likely to have another more recent browser they can use instead)

Microsoft no longer supports browsers other than IE11 for the platforms that is available for:

* https://www.microsoft.com/en-gb/WindowsForBusiness/End-of-IE-support
* https://support.microsoft.com/en-gb/lifecycle#gp/Microsoft-Internet-Explorer

This means that to all intents and purposes, for secure applications, it is not worth developing for IE 7-10.

Almost all other browsers are switiching to evergreen mode anyway. This means they automatically upgrade themselves to the latest versions anyway.

## Progressive Enhancement, Accessibility, Responsiveness

These are still important considerations in the modern web and are worth
designing for.

