# Dependencies

This is where project dependenices are cached, so that if the current pacakge
servers ever go down, or the maintainer removes the project, we can still
install and run our product.

If you are building this directory from scratch, first run this to bootstrap
the tools you need to build the cache:

```
make bootstrap
```
