# Release Candidates

This is where release candidates are put when they are generated.

The contents of this folder are not kept in source control (apart from this file).

See [Release Candidates](../docs/technical/candidate.md) for more information.
