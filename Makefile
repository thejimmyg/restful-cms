include config.mk

.PHONY: deps install build build/sql build/static build/log run
all: deps install build run-test test
deps:
	cd deps && make
install:
	cd install && make

run: build
	cd build && bin/cbl -config ../config/run.ini -etagPrefix=`date "+%s"`:
run-test: build
	cd build && bin/cbl -config ../config/run-test.ini
test: export CBL_GIT_ROOT=$(PWD)
test:
	./src/test/test.sh

build-clean:
	rm -rf build
build: build/files build/static build/sql build/bin build/tmpl build/config
build/config: build/config/run.ini build/config/run-test.ini
build/config/run.ini: $(cbl_config_file)
	mkdir -p build/config
	cp $< $@
build/config/run-test.ini: $(cbl_config_file)
	mkdir -p build/config
	cp $< $@
build/sql:
	rsync -aHxv src/sql build/
build/files: build/files/app.pjax.js build/files/app.js build/files/app.css
build/static: build/static/hello.txt
build/static/hello.txt:
	mkdir -p build/static
	echo "hello" > build/static/hello.txt
build/files/app.pjax.js: deps/browser/jquery-2.2.1.js deps/browser/jquery.pjax.js
	mkdir -p build/files
	cat deps/browser/jquery-2.2.1.js deps/browser/jquery.pjax.js > build/files/app.pjax.js
	echo '$$(document).pjax("a", "#pjax-container", {"fragment":"#pjax-container"})' >> build/files/app.pjax.js
build/files/app.js:
	echo '/* JS */' > build/files/app.js
build/files/app.css:
	echo '/* CSS */' > build/files/app.css
build/tmpl: $(shell find src/server/tmpl)
	rsync -aHxv src/server/tmpl build/
build/bin: build/bin/cbl
build/bin/cbl: export GOPATH=$(shell realpath install/goroot)
build/bin/cbl:
	mkdir -p build/bin
	# Note: This is actually building in the install directory, with the cbl source symlinked to the src directory.
	go install cbl/cbl
	cp install/goroot/bin/cbl build/bin/cbl
build/log:
	mkdir -p log

