# JSON API

## `GET /`

Gets a page where it used to be

* Accept: application/json

---

* Status: `302`
* Content-Type: application/json
* Location: http://localhost:8071/old


## `GET /`

* `?format=json`

Gets a page where it used to be

---

* Status: `302`
* Content-Type: application/json
* Location: http://localhost:8071/old


## `GET /old`

Gets a page that has been moved

* Accept: application/json

---

* Content-Type: application/json
* Status: `200`
* Etag: "1:1"
* Last-Modified: "Thu, 18 Feb 2016 22:33:19 GMT"

```
{"title":"d","body":"","format":"html"}
```

## `GET /old`

* `?format=json`

Gets a page that has been moved

---

* Content-Type: application/json
* Status: `200`
* Etag: "1:1"
* Last-Modified: "Thu, 18 Feb 2016 22:33:19 GMT"

```
{"title":"d","body":"","format":"html"}
```

## `GET /old`

* `?format=json`

Gets a page that has been moved

* If-Modified-Since: "Thu, 18 Feb 2016 22:33:19 GMT"

---

* Status: `304`

```
```

## `GET /old`

* Accept: application/json

Gets a page that has been moved

* If-Modified-Since: "Thu, 18 Feb 2016 22:33:19 GMT"

---

* Status: `304`

```
```

## `GET /old`

* `?format=json`

Gets a page that has been moved

* If-None-Match: "1:1"

---

* Status: `304`

```
```

## `GET /old`

* Accept: application/json

Gets a page that has been moved

* If-None-Match: "1:1"

---

* Status: `304`

```
```
