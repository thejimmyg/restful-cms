# Static File Support

## `GET /app.css`

Load the CSS file.

---

* Status: `200`
* Last-Modified: "Sun, 14 Feb 2016 23:16:15 GMT"

```
/* CSS */

```

## `GET /app.js`

Load the JS file.

---

* Status: `200`
* Last-Modified: "Sun, 14 Feb 2016 23:16:22 GMT"

```
/* JS */

```

## `GET /static/hello.txt`

Load the JS file.

---

* Status: `200`
* Last-Modified: "Sun, 14 Feb 2016 23:15:52 GMT"

```
hello

```
