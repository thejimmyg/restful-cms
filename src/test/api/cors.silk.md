# CORS Support

## `OPTIONS /old`

Make a CORS pre-flight check with no Origin

* Access-Control-Request-Headers: SomeHeader, SomeOtherHeader
* Access-Control-Request-Method: SomeMethod, SomeOtherMethod

---

* Status: `400`

## `OPTIONS /old`

Make a CORS pre-flight check with an invalid origin

* Origin: Invalid
* Access-Control-Request-Headers: SomeHeader, SomeOtherHeader
* Access-Control-Request-Method: SomeMethod, SomeOtherMethod

---

* Status: `400`


## `OPTIONS /old`

Make an OPTIONS pre-flight CORS request with a valid origin

* Origin: `http://example.com`
* Access-Control-Request-Headers: SomeHeader, SomeOtherHeader
* Access-Control-Request-Method: SomeMethod, SomeOtherMethod

---

* Status: `200`
* Access-Control-Allow-Credentials: "true"
* Access-Control-Allow-Methods: `SOMEMETHOD, SOMEOTHERMETHOD`
* Access-Control-Allow-Origin: `http://example.com`
* Access-Control-Max-Age: "1000"
* Content-Length: "0"
* Access-Control-Allow-Headers: `SomeHeader, SomeOtherHeader`
* Vary: `Origin, Access-Control-Request-Method, Access-Control-Request-Headers`

## `GET /old`

Gets a page that has been moved

* Accept: application/json
* Origin: http://example.com

---

* Access-Control-Allow-Origin: http://example.com
* Content-Type: application/json
* Status: `200`
* Etag: "1:1"
* Last-Modified: "Thu, 18 Feb 2016 22:33:19 GMT"

```
{"title":"d","body":"","format":"html"}
```
