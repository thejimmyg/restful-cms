# Cache API

## `GET /old`

Gets a page that has been moved

---

* Status: `200`
* Content-Type: text/html
* Etag: "1:1"
* Last-Modified: "Thu, 18 Feb 2016 22:33:19 GMT"
* Body: /<h1>d</h1>/


## `GET /old`

Gets a page that has been moved using just the Last-Modified date

* If-Modified-Since: "Thu, 18 Feb 2016 22:33:19 GMT"

---

* Status: `304`

```
```

## `GET /old`

Gets a page that has been moved using just the Etag value

* If-None-Match: "1:1"

---

* Status: `304`

```
```

## `GET /old`

Gets a page that has been moved using just valid Etag and LastModified

* If-Modified-Since: "Thu, 18 Feb 2016 22:33:19 GMT"
* If-None-Match: "1:1"

---

* Status: `304`

```
```


## `GET /old`

Gets a page that has been moved using an invalid Etag and valid LastModified (this might happen if the -etagPrefix option is changed). In this case we want a full response:

* If-Modified-Since: "Thu, 18 Feb 2016 22:33:19 GMT"
* If-None-Match: "2:1"

---

* Status: `200`
* Content-Type: text/html
* Etag: "1:1"
* Last-Modified: "Thu, 18 Feb 2016 22:33:19 GMT"
* Body: /<h1>d</h1>/
