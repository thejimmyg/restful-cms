# Login API

## `POST /login`

Invalid credentials

```
username=james&password=password
```
---

* Status: `401`
* Content-Type: text/html
* Body: /<h1>Error</h1>/
* Body: /Could not sign in with those credentials/

// Post body doesn't seem to get sent yet, bug in Silk. Test in the behavior
// tests anyway.
// ## `POST /login`
//
// Valid credentials
//
// ```
// username=enc1&password=enc1
// ```
// ---
//
// * Status: `200`
// * Content-Type: text/html
// * Body: /<h1>Success</h1>/
//
