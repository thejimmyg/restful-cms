# HTML API

## `GET /`

Gets a page where it used to be

---

* Status: `302`
* Content-Type: text/html
* Location: http://localhost:8071/old

## `GET /not-found`

---

* Status: `404`
* Content-Type: text/html
* Body: /Error/
* Body: /Not found/

## `GET /old`

Gets a page that has been moved

---

* Status: `200`
* Content-Type: text/html
* Etag: "1:1"
* Last-Modified: "Thu, 18 Feb 2016 22:33:19 GMT"
* Body: /<h1>d</h1>/



## `GET /old`

Gets a page that has been moved

* If-Modified-Since: "Thu, 18 Feb 2016 22:33:19 GMT"

---

* Status: `304`

```
```

## `GET /old`

Gets a page that has been moved

* If-None-Match: "1:1"

---

* Status: `304`

```
```
