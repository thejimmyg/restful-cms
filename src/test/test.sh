#!/bin/bash

set -e

if [ -z $CBL_GIT_ROOT ] ; then 
    echo "\$CBL_GIT_ROOT not set\n"
    exit 1
fi

GOPATH=`realpath "${CBL_GIT_ROOT}/install/goroot"`
CBLDB=james2
PORT=8071

touch -mt 201602142316.15 "$CBL_GIT_ROOT/build/files/app.css"
touch -mt 201602142316.22 "$CBL_GIT_ROOT/build/files/app.js"
touch -mt 201602142315.52 "$CBL_GIT_ROOT/build/static/hello.txt"

echo "Running Go tests ..."
go test -v $GOPATH/src/cbl/cbl_test.go
echo "done"

echo "Running API tests ..."
$GOPATH/bin/silk -silk.url="http://localhost:$PORT" $CBL_GIT_ROOT/src/test/api/static.silk.md
$GOPATH/bin/silk -silk.url="http://localhost:$PORT" $CBL_GIT_ROOT/src/test/api/html.silk.md
$GOPATH/bin/silk -silk.url="http://localhost:$PORT" $CBL_GIT_ROOT/src/test/api/json.silk.md
$GOPATH/bin/silk -silk.url="http://localhost:$PORT" $CBL_GIT_ROOT/src/test/api/cors.silk.md
$GOPATH/bin/silk -silk.url="http://localhost:$PORT" $CBL_GIT_ROOT/src/test/api/gzip.silk.md
$GOPATH/bin/silk -silk.url="http://localhost:$PORT" $CBL_GIT_ROOT/src/test/api/login.silk.md
$GOPATH/bin/silk -silk.url="http://localhost:$PORT" $CBL_GIT_ROOT/src/test/api/cache.silk.md
echo "done"
echo "Running browser tests ..."
mkdir -p $CBL_GIT_ROOT/build/log/screenshot
LOGFILE=$CBL_GIT_ROOT/build/log/test.console.log NODE_PATH=$CBL_GIT_ROOT/install/node_modules SCREENSHOT_PATH=$CBL_GIT_ROOT/build/log/screenshot HOST=http://localhost:8071 $CBL_GIT_ROOT/install/node_modules/.bin/cucumber.js -f pretty $CBL_GIT_ROOT/src/test/browser/features/*.feature 2> $CBL_GIT_ROOT/build/log/test.log
echo "done"
echo "All passed. Success."
