Feature: Basic Functionality

  Scenario: View a page
    When I navigate to /old
    Then 'd' is visible in 'h1'

  Scenario: Get redirected when a page moves
    When I navigate to /
    Then the browser moves to /old

  Scenario: Not found error
    When I navigate to /not-found
    Then 'Error' is visible in 'h1'
    And 'Not found' is visible in 'div.error p'

  Scenario: Failed login
    Given I navigate to /login
    And I have typed 'user_c04' into #username
    And I have typed 'password_c04' into #password
    When I click the button labelled 'Login'
    Then 'Error' is visible in 'h1'

  Scenario: Successful login
    Given I navigate to /login
    And I have typed 'enc1' into #username
    And I have typed 'enc1' into #password
    When I click the button labelled 'Login'
    Then 'Success' is visible in 'h1'
