select * from cbl_locator;
INSERT INTO cbl_locator (domain, slug, type, id) values ('localhost', '/', 'page', 1);
DELETE FROM cbl_locator WHERE slug = '/';
INSERT INTO cbl_locator (domain, slug, type, id) values ('localhost', '/e', 'page', 2);
INSERT INTO cbl_locator (domain, slug, type, id) values ('localhost', '/old', 'page', 1);
select * from cbl_locator;
