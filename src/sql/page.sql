CREATE EXTENSION "uuid-ossp";
CREATE TABLE section (
    id BIGINT DEFAULT cbl_resource_id(),
    name VARCHAR
);
-- ALTER TABLE section ADD CONSTRAINT section_unique_src UNIQUE (lang, src);
CREATE TRIGGER section_audit_trigger AFTER INSERT OR UPDATE OR DELETE ON section
    FOR EACH ROW EXECUTE PROCEDURE cbl_audit();
CREATE TRIGGER section_redirect_trigger AFTER INSERT OR UPDATE OR DELETE ON section
    FOR EACH ROW EXECUTE PROCEDURE cbl_resource_redirect();


CREATE TABLE page (
    id BIGINT DEFAULT cbl_resource_id(),
    lang VARCHAR NOT NULL DEFAULT '',
    publish_from TIMESTAMP DEFAULT  '1970-01-01 01:00:00+01',
    publish_to TIMESTAMP DEFAULT '9999-12-24 23:59:59',
    src VARCHAR, -- Can be NULL to mean we don't care about the src, and not to use the constraint
    format VARCHAR NOT NULL DEFAULT 'html',
    -- Properties
    title VARCHAR NOT NULL DEFAULT '',
    -- default page
    body VARCHAR NOT NULL DEFAULT ''
);
ALTER TABLE page ADD CONSTRAINT page_unique_src UNIQUE (src);
CREATE TRIGGER page_audit_trigger AFTER INSERT OR UPDATE OR DELETE ON page
    FOR EACH ROW EXECUTE PROCEDURE cbl_audit();
CREATE TRIGGER page_redirect_trigger AFTER INSERT OR UPDATE OR DELETE ON page
    FOR EACH ROW EXECUTE PROCEDURE cbl_resource_redirect();
CREATE TRIGGER page_revise_section_trigger AFTER UPDATE OR DELETE ON child
    FOR EACH ROW EXECUTE PROCEDURE cbl_revise_parent_trigger('section');


CREATE TABLE section_relation (
   parent_type VARCHAR NOT NULL,
   parent_id BIGINT NOT NULL,
   child_type VARCHAR NOT NULL,
   child_id BIGINT NOT NULL,
   default_child BOOLEAN NOT NULL DEFAULT false
);
CREATE TRIGGER section_relation_audit_relationship_for_parent_trigger BEFORE INSERT OR UPDATE OR DELETE ON section_relation
    FOR EACH ROW EXECUTE PROCEDURE cbl_relate_for_parent();


CREATE TABLE session (
    session UUID DEFAULT uuid_generate_v4(),
    csrf UUID DEFAULT uuid_generate_v4(),
    login_attempt BIGINT NOT NULL,
    logout TIMESTAMP
);
-- -- ALTER TABLE session ADD CONSTRAINT session_unique_src UNIQUE (lang, src);
-- CREATE TRIGGER session_audit_trigger AFTER INSERT OR UPDATE OR DELETE ON session
--     FOR EACH ROW EXECUTE PROCEDURE cbl_audit();
-- CREATE TRIGGER session_redirect_trigger AFTER INSERT OR UPDATE OR DELETE ON session
--     FOR EACH ROW EXECUTE PROCEDURE cbl_resource_redirect();
-- -- CREATE TRIGGER XXX_revise_session_trigger AFTER UPDATE OR DELETE ON child
-- --     FOR EACH ROW EXECUTE PROCEDURE cbl_revise_parent_trigger('section');
-- ALTER TABLE session ENABLE ROW LEVEL SECURITY;
-- CREATE POLICY user_policy ON session
--     USING (true)
--     WITH CHECK (user = current_user);

-- CREATE USER guest WITH PASSWORD 'guest';
-- SELECT usename FROM pg_shadow WHERE passwd=concat('md5', md5(concat('guest', 'guest')));
-- SET ROLE guest;
-- INSERT INTO session (username) VALUES ('guest')
-- SET ROLE james;
-- CREATE ROLE name;
-- Typically a role being used as a group would not have the LOGIN attribute, though you can set it if you wish.
--
-- Once the group role exists, you can add and remove members using the GRANT and REVOKE commands:
--
-- GRANT group_role TO role1, ... ;
-- REVOKE group_role FROM role1, ... ;

--
-- Set up the data
--
INSERT INTO page (id, title) VALUES (1, 'd');
-- Set some values for the tests
UPDATE cbl_change SET tstamp = '2016-02-18 22:33:18.20378' WHERE operation='RELATE';
UPDATE cbl_change SET tstamp = '2016-02-18 22:33:19.27919' WHERE revision =1;
SELECT * FROM cbl_change;

CREATE TABLE cbl_login_attempt (
    id BIGSERIAL,
    username VARCHAR NOT NULL,
    ip VARCHAR NOT NULL,
    at TIMESTAMP DEFAULT NOW()
);
INSERT INTO page (id, title, body) VALUES (2, 'e', 'Hello world!');




CREATE TABLE templatedpage (
    id BIGINT DEFAULT cbl_resource_id(),
    lang VARCHAR NOT NULL DEFAULT '',
    publish_from TIMESTAMP DEFAULT  '1970-01-01 01:00:00+01',
    publish_to TIMESTAMP DEFAULT '9999-12-24 23:59:59',
    src VARCHAR, -- Can be NULL to mean we don't care about the src, and not to use the constraint
    format VARCHAR NOT NULL DEFAULT 'html',
    -- Properties
    title VARCHAR NOT NULL DEFAULT ''
    -- body VARCHAR NOT NULL DEFAULT ''
);
ALTER TABLE templatedpage ADD CONSTRAINT templatedpage_unique_src UNIQUE (src);
CREATE TRIGGER templatedpage_audit_trigger AFTER INSERT OR UPDATE OR DELETE ON templatedpage
    FOR EACH ROW EXECUTE PROCEDURE cbl_audit();
CREATE TRIGGER templatedpage_redirect_trigger AFTER INSERT OR UPDATE OR DELETE ON templatedpage
    FOR EACH ROW EXECUTE PROCEDURE cbl_resource_redirect();
CREATE TRIGGER templatedpage_revise_section_trigger AFTER UPDATE OR DELETE ON child
    FOR EACH ROW EXECUTE PROCEDURE cbl_revise_parent_trigger('section');

CREATE TABLE templatedpage_relation (
   parent_type VARCHAR NOT NULL,
   parent_id BIGINT NOT NULL,
   child_type VARCHAR NOT NULL,
   child_id BIGINT NOT NULL
);
CREATE TRIGGER templatedpage_relation_audit_relationship_for_parent_trigger BEFORE INSERT OR UPDATE OR DELETE ON templatedpage_relation
    FOR EACH ROW EXECUTE PROCEDURE cbl_relate_for_parent();

CREATE TABLE region (
    id BIGINT DEFAULT cbl_resource_id(),
    lang VARCHAR NOT NULL DEFAULT '',
    publish_from TIMESTAMP DEFAULT  '1970-01-01 01:00:00+01',
    publish_to TIMESTAMP DEFAULT '9999-12-24 23:59:59',
    src VARCHAR, -- Can be NULL to mean we don't care about the src, and not to use the constraint
    format VARCHAR NOT NULL DEFAULT 'html',
    -- Properties
    title VARCHAR NOT NULL DEFAULT ''
    -- body VARCHAR NOT NULL DEFAULT ''
);
ALTER TABLE region ADD CONSTRAINT region_unique_src UNIQUE (src);
CREATE TRIGGER region_audit_trigger AFTER INSERT OR UPDATE OR DELETE ON region
    FOR EACH ROW EXECUTE PROCEDURE cbl_audit();
CREATE TRIGGER region_redirect_trigger AFTER INSERT OR UPDATE OR DELETE ON region
    FOR EACH ROW EXECUTE PROCEDURE cbl_resource_redirect();
CREATE TRIGGER region_revise_section_trigger AFTER UPDATE OR DELETE ON child
    FOR EACH ROW EXECUTE PROCEDURE cbl_revise_parent_trigger('templatedpage');
