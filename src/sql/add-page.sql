\echo :'src'
\echo :'title'
-- \echo :'content'
\echo :'format'
\echo :'domain'
\echo :'slug'

WITH pages AS (
   INSERT INTO page (src, format, title, body) VALUES (:'src', :'format', :'title', :'content')
   ON CONFLICT (src) DO UPDATE SET body=:'content', format=:'format', title=:'title'
   RETURNING id
)

INSERT INTO cbl_locator (domain, slug, type, id) values (
    :'domain',
    :'slug', 
    'page',
    (select id from pages)
)
ON CONFLICT (domain, slug) DO UPDATE set type='page', id=(select id from pages)
;
