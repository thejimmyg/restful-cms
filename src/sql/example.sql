CREATE TABLE child (
    id BIGINT DEFAULT cbl_resource_id(),
    src VARCHAR, -- Can be NULL to mean we don't care about the src, and not to use the constraint
    title VARCHAR NOT NULL DEFAULT ''
);
ALTER TABLE child ADD CONSTRAINT child_unique_src UNIQUE (src);
CREATE TRIGGER child_audit_trigger AFTER INSERT OR UPDATE OR DELETE ON child
    FOR EACH ROW EXECUTE PROCEDURE cbl_audit();
CREATE TRIGGER child_redirect_trigger AFTER INSERT OR UPDATE OR DELETE ON child
    FOR EACH ROW EXECUTE PROCEDURE cbl_resource_redirect();
CREATE TRIGGER child_revise_parent_trigger AFTER UPDATE OR DELETE ON child
    FOR EACH ROW EXECUTE PROCEDURE cbl_revise_parent_trigger('parent');

CREATE TABLE parent (
    id BIGINT DEFAULT cbl_resource_id(),
    title VARCHAR NOT NULL DEFAULT ''
);
CREATE TRIGGER parent_audit_trigger AFTER INSERT OR UPDATE OR DELETE ON parent
    FOR EACH ROW EXECUTE PROCEDURE cbl_audit();
CREATE TRIGGER parent_redirect_trigger AFTER INSERT OR UPDATE OR DELETE ON parent
    FOR EACH ROW EXECUTE PROCEDURE cbl_resource_redirect();

CREATE TABLE parent_relation (
   parent_type VARCHAR NOT NULL,
   parent_id BIGINT NOT NULL,
   child_type VARCHAR NOT NULL,
   child_id BIGINT NOT NULL,
   default_child BOOLEAN NOT NULL DEFAULT false
);
CREATE TRIGGER parent_relation_audit_relationship_for_parent_trigger BEFORE INSERT OR UPDATE OR DELETE ON parent_relation
    FOR EACH ROW EXECUTE PROCEDURE cbl_relate_for_parent();


INSERT INTO child (id, title) VALUES (1, 'a');
INSERT INTO child (id, title) VALUES (2, 'b');
UPDATE child SET title = 'c' WHERE id = 1;
DELETE FROM child WHERE id = 2;

SELECT * FROM child;
SELECT * FROM cbl_change;

SELECT * from cbl_redirect WHERE domain='jimmyg.org' AND slug = '/' ORDER BY revision DESC, redirect DESC;
INSERT INTO child (id, title) VALUES (2, 'b');
SELECT * from cbl_redirect WHERE domain='jimmyg.org' AND slug = '/' ORDER BY revision DESC, redirect DESC;


INSERT INTO parent (id, title) VALUES (1, 's');
INSERT INTO parent_relation (parent_type, parent_id, child_type, child_id, default_child) VALUES ('parent', 1, 'child', 1, false);
UPDATE parent_relation SET default_child = false;
UPDATE child SET title = 'd' WHERE id = 1;
DELETE FROM parent_relation;

SELECT * FROM parent;
SELECT * FROM parent_relation;
SELECT * FROM cbl_change;
SELECT * FROM cbl_redirect;
