-- # WebDB content model
--
-- Designed to produce simple JSON structutres for API use, provide a rich query
-- model, support a rich linking and moving model, provide useful change tracking,
-- and a caching model.
--
-- There are three types of "thing" in the database:
--
-- * Resource - an entity like a blog post or comment, has an ID
-- * Relationship - describes how two resources are related, must not have an
--   ID (otherwise it is really a resource, with other relationships)
-- * Helper functions, tables and sequences - do useful things to help find
--   resources and track their changes
--
-- Let's start by describing the helper tables, functions and sequences then it
-- will be clearer what the other part
--
-- ## Sequences and Functions
--
-- We need two sequences, both of which start at 1:
--
-- `cbl_id`
-- :   generates unique IDs which can be combined with other data to produce
--     internal `id` values for resources by the `cbl_resource_id()` function
-- `cbl_revision_id`
-- :   generates unique IDs for the internal revision numbers
-- `cbl_redirect_id`
-- :   used to make sure we can tell which of two redirects to the same
--     resource ID, set up in the same transaction is the most recet.

CREATE SEQUENCE cbl_id START 1;
CREATE SEQUENCE cbl_revision_id START 1;
CREATE SEQUENCE cbl_redirect_id START 1;

-- In each transaction, the calling code should increment `cbl_revision_id`


-- In order to actually generate `id` values for resources we want to combine
-- the next available `cbl_id` with other information that could be used for
-- easy time ordering or sharding later. We follow the approach chosen by
-- Instagram and described at https://news.ycombinator.com/item?id=3058327 to
-- create:
--
-- `cbl_resource_id`
-- :   A function that uses the next available `cbl_id` and combines it
--     with timestamp and shard information to create a BIGINT suitable
--     for use as an internal resource `id`.
-- `cbl_current_seq_value`
-- :   Used in triggers to use the current value, or increment if
--     undefined

CREATE OR REPLACE FUNCTION cbl_current_seq_value(seq regclass) RETURNS INTEGER language plpgsql
AS $$
BEGIN
    BEGIN
        RETURN (SELECT currval(seq));
    EXCEPTION
        WHEN sqlstate '55000' then return (SELECT nextval(seq));
    END;
END $$;

SELECT cbl_current_seq_value('cbl_revision_id');
SELECT cbl_current_seq_value('cbl_revision_id');

CREATE OR REPLACE FUNCTION cbl_resource_id(OUT result bigint) AS $$
DECLARE
    our_epoch bigint := 1314220021721;
    seq_id bigint;
    now_millis bigint;
    shard_id int := 5;
BEGIN
    SELECT nextval('cbl_id') % 1024 INTO seq_id;

    SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000) INTO now_millis;
    result := (now_millis - our_epoch) << 23;
    result := result | (shard_id << 10);
    result := result | (seq_id);
END;
$$ LANGUAGE PLPGSQL;


-- ## Tables, Constraints and Indexes
--
-- At this point it is worth having a brief summary of the internal tables
-- that we'll be using:
--
-- `cbl_locator`
-- :   maps a `domain` and `slug` (maybe from a URL) to a resource's `type` and
--     `id`
-- `cbl_redirect`
-- :   updated with a locator's `domain` and `slug` change to track the
--     old values in case people use them later. Stores when the change
--     happened. Has a `redirect` column to distinguish which update
--     in a single transaction was the most recent.
-- `cbl_resource_cache`
-- :   can hold the latest revision for a particular type and ID, as well as an
--     etag of its public JSON (if generated), but this is not automatically
--     maintained.
-- `cbl_change`
-- :   tracks changes to resources as well as to their related resources
--     (linked, dependant or pinned)
-- `cbl_migrate`
-- :   stores the version of the cbl setup we are using
--
-- None of the columns in these tables are allowed NULL values, instead
-- optional ones might take a default empty value.
--
-- We also want to add some constraints:
-- `cbl_unique_id_[locator, resource_cache]`
-- :   Resource `id` values should be unique across the database,
--     not just in a particular resource table, so we enforce this
--     in the `cbl_locator` and `cbl_resource_cache` tables
-- `cbl_unique_domain_and_slug_locator`
-- :   We can't have two different resources for the same domain
--     and slug. This is enforced in the `cbl_locator` table
-- `cbl_unique_type_and_id_revision`
-- :   We can't have more than one revision row for the same `type`
--     and `id`
--
-- And some indexes for faster lookups:
-- `cbl_domain_and_slug_index_[locator, redirect]`
-- :   Make it fast to find the `type` and `id` associated with a `domain` and `slug`
-- `cbl_type_and_id_index_[change, revision, locator]`
-- :   Make it fast to find the `revision` from a `type` and `id`
-- `cbl_revision_index_change`
-- :   Make it fast to find the latest revision of an entity that isn't deleted.
-- `cbl_operation_index_change`
-- :   Make it fast to find the latest revision of an entity that isn't deleted.


CREATE TABLE cbl_locator (
    domain VARCHAR NOT NULL,
    slug VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    id BIGINT NOT NULL,
    weight INTEGER DEFAULT 0
);
ALTER TABLE cbl_locator ADD CONSTRAINT cbl_unique_id_locator UNIQUE (id);
ALTER TABLE cbl_locator ADD CONSTRAINT cbl_unique_domain_and_slug_locator UNIQUE (domain, slug);
CREATE INDEX cbl_domain_and_slug_index_locator ON cbl_locator (domain, slug);
CREATE INDEX cbl_type_and_id_index_locator ON cbl_locator (type, id);


CREATE TABLE cbl_redirect (
    domain VARCHAR NOT NULL,
    slug VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    id BIGINT,
    revision BIGINT NOT NULL,
    redirect BIGINT NOT NULL,
    resource_exists BOOLEAN NOT NULL DEFAULT TRUE
);
CREATE INDEX cbl_domain_and_slug_index_redirect ON cbl_locator (domain, slug);


CREATE TABLE cbl_migrate (version INT NOT NULL);
INSERT INTO cbl_migrate (version) VALUES (1);


CREATE TABLE cbl_resource_cache (
    type VARCHAR NOT NULL,
    id BIGINT NOT NULL,
    public_json VARCHAR NOT NULL,
    unchanged_since_revision BIGINT NOT NULL,
    public_json_etag VARCHAR NOT NULL
);
ALTER TABLE cbl_resource_cache ADD CONSTRAINT cbl_unique_id_resource_cache UNIQUE (id);
ALTER TABLE cbl_resource_cache ADD CONSTRAINT cbl_unique_type_and_id_resource_cache UNIQUE (type, id);
CREATE INDEX cbl_type_and_id_resource_cache ON cbl_resource_cache (type, id);


CREATE TABLE cbl_change (
    revision   BIGINT NOT NULL, -- the revision ID
    tstamp     timestamp DEFAULT statement_timestamp(),
    txid       BIGINT DEFAULT txid_current(),
    type       VARCHAR NOT NULL,
    id         BIGINT, -- the resource `id` of the `type`
    operation  text,
    who        text DEFAULT current_user,
    old_val    json NOT NULL,
    new_val    json NOT NULL
);
CREATE INDEX cbl_type_and_id_index_change ON cbl_change (type, id);
CREATE INDEX cbl_revision_index_change ON cbl_change (revision);
CREATE INDEX cbl_operation_index_change ON cbl_change (operation);


-- ## Resources and Relationships
--
-- Now that you can see what is being managed, let's look at Resources and
-- relationships.
--
-- There are three types of relationship possible:
--
-- * Link
-- * Depend
-- * Pin
--
-- All types of relationship are about adding or removing a child to a parent. For
-- all types children can be shared by more than one parent of more than one type.
--
-- For example, thinking of website sections and pages, a section might have zero
-- pages, one page or many pages. You might add or remove a page from a section.
-- Any time you add or remove a page from a section, the section will have an
-- APPEND or REMOVE event added to its history, and its revision will be updated.
--
-- The differences between `depend`, `link` and `pin` relationships all come down
-- to what the child in the relationship is and how it is referenced.
--
-- Link
-- :   For a link relationship, the child is referenced by a domain and slug. To get
--     whatever the child resolves to requires a separate database query. When a
--     linked-child changes, the parent knows nothing about it. If a linked child is
--     moved or deleted, the parent won't know until it tries to get it.
--
-- Depend
-- :   For a depend relationship, the child is referenced by its resource type and
--     internal ID. Whenever the child changes, a custom trigger will be run to see if
--     the change affects the parent. If it does, the parent will have a REVISE event
--     logged in its history and its revision will also be updated. If a dependant
--     child is deleted, a REMOVE event will be automatically applied to the parent,
--     which will cause its revision to be updated.
--
-- Pin
-- :   A pin relationship is like a `Depend` relationship, except it links to a
--     specific revision of the child using resource type, id and revision. Since that
--     specific revision can never be deleted or changed, parents of pinned children
--     will never get REVISE events. To look up the pinned child, requires a query on
--     the history table, and the data returned will be JSON not rows. Pinned
--     resources are really just designed for APIs that want to checkpoint how
--     something was at a particular time, not for querying it at that point.
--
-- In any of these cases you'll need a relationship table to store the relationship.
-- Since parents often have multiple child types (for example a page having comments
-- and authors), it makes sense to structure the tables with a `parent_type`,
-- `parent_id`, `child_type` and `child_id` along with any other fields.
--
--
-- ## Trigger-Generating Functions
--
-- We use *triggers* to keep the `cbl` tables up to date as data is changing in
-- both the internal tables, and the resource and relationship tables in your
-- model.
--
-- Let's start by looking at the functions that create the triggers that need
-- to exist:
--
-- `cbl_locator_redirect()`
-- :   An internal trigger used to update the `cbl_redirect` table if changes
--     are made to the `cbl_locator` table
-- `cbl_resource_redirect()`
-- :   An resource trigger to update `cbl_redirect` about whether or not the
--     located resource exists or not
-- `cbl_audit()`
-- :   Simply tracks all INSERT, UPDATE and DELETE operations and ensures that the
--     correct information is added to the `cbl_change` table. This is used
--     internally to track changes on the `cbl_locator` table, but can also be
--     used on resource tables to track their changes. Don't use it on relationship
--     tables - `cbl_relate_for_parent` is for that.
-- `cbl_relate_for_parent() table`
-- :   Tracks changes to fields in the relationship that aren't the two
--     foreign keys. Also enforces that these can't be updated. If you want
--     to remove a relationship, it should be DELETED, adding a new one should
--     be done with INSERT. It is OK to change other fields though.
--     When a row in the child resource is created with INSERT or removed
--     with DELETE, the parent type will receive a corresponding `RELATE`
--     or `UNRELATE` operation. When a non-relationship fields in the child
--     resource is changed with an UPDATE, the parent receives a
--     `RELATION_CHANGE` event.
-- `*_revise(parent_resource_type, child_resource_type)`
-- :   Write your own trigger that issues `REVISE` events for all the parents
--     the child knows care.
--
-- Note: Although the `cbl_audit` trigger is used internally on the `cbl_locator`
-- table, none of the other `cbl_*` tables need to have their changes tracked
-- because their content could be re-generated from the information in
-- `cbl_changes` table.
--

-- Let's look a bit more at cbl_locator_redirect(). We don't want people to be
-- able to change the domain or slug, as these are effectively the locator
-- so they should delete the old and create the new if that's what they want
-- to do. This is automatically set up for you on `cbl_locator`.


CREATE OR REPLACE FUNCTION cbl_locator_redirect() RETURNS trigger AS $$
BEGIN
    -- IF ((TG_OP = 'UPDATE') ) THEN
--AN-- D ((NEW.domain!= OLD.domain) OR NEW.slug != OLD.slug)) THEN
    --     RAISE EXCEPTION 'cbl error: cannot update domain or slug in cbl_locator, delete the old locator and create a new one to keep the cbl_redirect table working';
    -- END IF;
    IF TG_OP = 'INSERT' THEN
        RAISE NOTICE 'INSERTING into cbl_redirect due to new locator';
        INSERT INTO cbl_redirect (domain, slug, type, id, revision, redirect)
            VALUES (NEW.domain, NEW.slug, NEW.type, NEW.id, cbl_current_seq_value('cbl_revision_id'), nextval('cbl_redirect_id'));
        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        RAISE NOTICE 'DELETING cbl_redirect due to updated locator';
        DELETE FROM cbl_redirect WHERE domain=OLD.domain AND slug=OLD.slug AND type=OLD.type AND id=OLD.id;
        RAISE NOTICE 'INSERTING into cbl_redirect due to new locator';
        INSERT INTO cbl_redirect (domain, slug, type, id, revision, redirect)
            VALUES (NEW.domain, NEW.slug, NEW.type, NEW.id, cbl_current_seq_value('cbl_revision_id'), nextval('cbl_redirect_id'));
        RETURN NEW;
      --IF NEW.type != OLD.type OR NEW.id != OLD.id) THEN
    --     RAISE NOTICE 'INSERTING into cbl_redirect due to locator target change';
    --     INSERT INTO cbl_redirect (domain, slug, type, id, revision, redirect)
    --         VALUES (NEW.domain, NEW.slug, NEW.type, NEW.id, currval('cbl_revision_id'), nextval('cbl_redirect_id'));
    ELSIF TG_OP = 'DELETE' THEN
        RETURN OLD;
        --RAISE NOTICE 'We do not do anything to the locator INSERTING into cbl_redirect due to locator target change';
        -- INSERT INTO cbl_redirect (domain, slug, type, id, revision, redirect)
        --     VALUES (NEW.domain, NEW.slug, NEW.type, NEW.id, currval('cbl_revision_id'), nextval('cbl_redirect_id'));
    END IF;
END;
$$ LANGUAGE 'plpgsql' SECURITY INVOKER;

CREATE TRIGGER cbl_locator_redirect_trigger BEFORE INSERT OR UPDATE OR DELETE ON cbl_locator
    FOR EACH ROW EXECUTE PROCEDURE cbl_locator_redirect();


-- For every resource table that a locator might point to you should also add
-- the `cbl_resource_redirect()` trigger to update the `cbl_redirect` table as
-- resources are created and deleted (and re-created).


CREATE OR REPLACE FUNCTION cbl_resource_redirect() RETURNS trigger AS $$
BEGIN
    IF TG_OP = 'UPDATE' AND NEW.id != OLD.id
    THEN
        RAISE EXCEPTION 'cbl error: cannot update id resource tables, delete the old resource and create a new one to keep the cbl_redirect table working';
    ELSIF TG_OP = 'DELETE'
    THEN
        RAISE NOTICE 'UPDATING cbl_redirect due to resource delete';
        UPDATE cbl_redirect SET resource_exists=false WHERE id=OLD.id AND type=TG_RELNAME;
        RETURN OLD;
    ELSIF TG_OP = 'INSERT'
    THEN
        RAISE NOTICE 'INSERTING into cbl_redirect due to new locator, and UPDATING any previously not existing';
        UPDATE cbl_redirect SET resource_exists=true WHERE id=NEW.id AND type=TG_RELNAME;
    ELSIF TG_OP = 'UPDATE'
    THEN
        -- Maybe not needed?
        RAISE NOTICE 'UPDATING cbl_redirect due to resource update';
        UPDATE cbl_redirect SET resource_exists=true WHERE id=NEW.id AND type=TG_RELNAME;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE 'plpgsql' SECURITY INVOKER;


CREATE OR REPLACE FUNCTION cbl_relate_for_parent() RETURNS trigger AS $$
DECLARE
    result bigint;
    BEGIN
        IF      TG_OP = 'UPDATE' AND (
            (NEW.parent_type != OLD.parent_type) OR
            (NEW.parent_id != OLD.parent_id) OR
            (NEW.child_type != OLD.child_type) OR
            (NEW.child_id != OLD.child_id))
        THEN
            RAISE NOTICE 'cbl_audit: You cannot change parent_type, parent_id, child_type or child_id of an audited relationship, please delete and insert instead';
        END IF;
        IF      TG_OP = 'INSERT'
        THEN
                INSERT INTO cbl_change (revision, type, id, operation, new_val, old_val)
                        VALUES (cbl_current_seq_value('cbl_revision_id'), NEW.parent_type, NEW.parent_id, 'RELATE', row_to_json(NEW), '{}');
                RETURN NEW;
        ELSIF   TG_OP = 'UPDATE'
        THEN
                INSERT INTO cbl_change (revision, type, id, operation, new_val, old_val)
                        VALUES (cbl_current_seq_value('cbl_revision_id'), NEW.parent_type, NEW.parent_id, 'RELATION_CHANGE', row_to_json(NEW), row_to_json(OLD));
                RETURN NEW;
        ELSIF   TG_OP = 'DELETE'
        THEN
                INSERT INTO cbl_change (revision, type, id, operation, new_val, old_val)
                        VALUES (cbl_current_seq_value('cbl_revision_id'), OLD.parent_type, OLD.parent_id, 'UNRELATE', '{}', row_to_json(OLD));
                RETURN OLD;
        END IF;
    END;
$$ LANGUAGE 'plpgsql' SECURITY INVOKER;


CREATE OR REPLACE FUNCTION cbl_audit() RETURNS trigger AS $$
DECLARE
    BEGIN
        IF      TG_OP = 'UPDATE' AND NEW.id != OLD.id
        THEN
            RAISE NOTICE 'cbl_audit: You cannot change the id of an audited resource';
        END IF;
        IF      TG_OP = 'INSERT'
        THEN
                INSERT INTO cbl_change (revision, type, id, operation, new_val, old_val)
                        VALUES (cbl_current_seq_value('cbl_revision_id'), TG_RELNAME, NEW.id, TG_OP, row_to_json(NEW), '{}');
                RETURN NEW;

        ELSIF   TG_OP = 'UPDATE'
        THEN
                INSERT INTO cbl_change (revision, type, id, operation, new_val, old_val)
                        VALUES (cbl_current_seq_value('cbl_revision_id'), TG_RELNAME, OLD.id, TG_OP, row_to_json(NEW), row_to_json(OLD));
                RETURN NEW;
        ELSIF   TG_OP = 'DELETE'
        THEN
                INSERT INTO cbl_change (revision, type, id, operation, new_val, old_val)
                        VALUES (cbl_current_seq_value('cbl_revision_id'), TG_RELNAME, OLD.id, TG_OP, '{}', row_to_json(OLD));
                RETURN OLD;
        END IF;
    END;
$$ LANGUAGE 'plpgsql' SECURITY INVOKER;


CREATE OR REPLACE FUNCTION cbl_revise_parent_trigger() RETURNS trigger AS $$
  DECLARE
      result bigint;
      parent varchar;
      BEGIN
          select TG_ARGV[0] into parent;
          IF      TG_OP = 'UPDATE' AND NEW.id != OLD.id
          THEN
              RAISE NOTICE 'cbl_audit: You cannot change the id of an audited resource';
          END IF;
          IF   TG_OP = 'UPDATE'
          THEN
                  EXECUTE $fun$
                      INSERT INTO cbl_change (revision, type, id, operation, new_val, old_val)
                      SELECT
                          cbl_current_seq_value('cbl_revision_id')
                        , relationship.parent_type
                        , relationship.parent_id
                        , 'REVISE',
                        $1,
                        $2
                      FROM $fun$ || quote_ident(parent) || $fun$_relation AS relationship
                      WHERE
                        relationship.parent_type = $3 AND
                        relationship.child_type = $4 AND relationship.child_id = $5;
                    $fun$
                  USING  row_to_json(NEW), row_to_json(OLD), parent, TG_RELNAME, OLD.id;
                  RETURN NEW;
          ELSIF   TG_OP = 'DELETE'
          THEN
                  -- We shouldn't be able to delete a node which still has things appended
                  EXECUTE $fun$
                  SELECT count(parent_id) FROM $fun$ || parent || $fun$_relation WHERE child_type=$1 AND child_id=$2
$fun$ USING TG_RELNAME, OLD.id INTO result;
                  IF result > 0 THEN
                      RAISE EXCEPTION 'cbl_ Cannot delete, has dependencies in %v_relation, remove them first', parent;
                  END IF;
                  RETURN OLD;
          END IF;
      END;
  $$ LANGUAGE 'plpgsql' SECURITY INVOKER;
