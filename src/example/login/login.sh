#!/bin/bash

CBL_SESSION_ID=`curl -v --data "username=enc1&password=enc1" http://localhost:8071/login 2>&1 | grep "< Set-Cookie: " |  tr " " "\n" | grep "session=" | tr "=" "\n" | grep ';'`
CBL_SESSION_ID=${CBL_SESSION_ID:0:${#CBL_SESSION_ID}-1}
echo $CBL_SESSION_ID


curl -v --cookie "session=$CBL_SESSION_ID"  http://localhost:8071/old?format=json 2>&1

