#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "sync.sh SRC_DIR"
    exit 1
fi

export START=$1
export CBLDB=james2

function pageadd {
    src=${1:(${#START}+1)} # Strip the characters from the input path
    echo "Adding $src ..."
    title=`cat $1 | head -n 1`
    content=`cat $1 | tail -n+2`
    psql --set=format="markdown" \
         --set=domain="localhost" \
         --set=src="jimmyg:$src" \
         --set=title="$title" \
         --set=slug="/$src" \
         --set=content="${content}" -f sql/add-page.sql $CBLDB 2>&1  >> sync.log
    echo "done..."
}

export -f pageadd
find $START \
-type f \( -name "*.md" \)  \
-exec bash -c '"$@"' pageadd pageadd '{}' \;
