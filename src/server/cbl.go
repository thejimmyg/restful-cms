/*
cbl is a package for building a RESTful web database that supports CORS, GZip,
Renaming, Etags, Last-Modified and Accept. Behind the scenes it is run from a
PostgreSQL database that uses triggers to maintain a full change history and a
revisioning mechanism so that parent resources get their revision updated when
child resources change. This is the basis for both auditing changes, and cache
invalidation. The server supports TLS and HTTP/2 on Go 1.6.

You can override the default format by appending `?format=json` to the URL. If
a resource is moved, the server will suggest a loaction to look for it based
on where the resource that used to be there has moved to. Again, this is tracked
behind the scenes using triggers in the PostgreSQL DB.

The tests for the package are written in Silk at the HTTP level.

Include arbitrary metadata such as whether the request was successful or what
the current location of the resource is. That is all sent in the HTTP protocol
so there is no point in repeating it. Internally, the data strucutres that
represent resources all follow the SuccessResponder() interface and will have
this information, it just won't get sent as part of the response body.

One consequence of this is that it is better if you make sure your resources
avoid using fields named error, revision, etag, domain, slug or type.

Read the `sql/web.sql` documentation for more information about the database
side of things. Particularly the naming of relationship tables which should
be something like `parent_relation`.

Internally three main classes of interface are used: Requester (which provides
useful objects that are likely to be used during the request processing),
SuccessResponder (which has methods for generating responses from data based on
the format and caching specified in the request) and ErrorResponder (which
implements the error interface but also contains information to generate an
error response, and an error log).

The internal request processing goes ServeHTTP -> HandleResource -> HandlePage.

We expect templates named base.html, error.html, login.html and page.html. The
last three inherit from base.html. All of them provide content and sidebar
blocks and expect a variable name .Title to use as the page title and heading.

New caching behaviour - if an etag is present use that. Otherwise use
If-Modified-Since but be aware that whilst the etags will change every time
etagPrefix changes, the If-Modified-Since value will not, so you might end up
caching things that have changed in theme, but not in content.


Next
====

[ ] Add some things with links in them e.g. docs directory
[ ] Get jQuery and PJAX into app.js

Backlog
=======

[ ] Write Gzip test

SQL:
[ ] Internally, we could make fields like _id, _parent_type, _parent_id,
    _child_type, _child_id all start with _ if they don't start cbl_

Accept:
[ ] Ability to render the .md file using text/html
[ ] Page language, vary on Accept-Language?

DB Security:
[ ] Row level security on things
[ ] Create guest user and set minimal set of rights to guest user

Page sections:
[ ] Changing a page title changes its section, and all pages in those sections

Advanced security:
[ ] Use an IP check in the session
[ ] Keep track of failed session requests and their IP


*/
package cbl

import (
	"bitbucket.org/ww/goautoneg"
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/microcosm-cc/bluemonday"
	"github.com/russross/blackfriday"
	"html/template"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"sync/atomic"
	"time"
)

var correlationID uint64

func init() {
	correlationID = 0
}

// NewCors creates an http.Handler that handles pre-flight OPTIONS and CORS
// requests. Of particular note, it always requires an `Origin` header, and
// will always use this as the Access-Control-Allow-Origin response header. It
// will use the value of "Access-Control-Request-Headers" and
// "Access-Control-Request-Method" in the response, without checking them. The
// methods will be capitalized.
func NewCors(h http.Handler, origins []string) *Cors {
	return &Cors{handler: h, origins: origins}
}

// Handler apply the CORS specification on the request, and add relevant CORS
// headers as necessary.
type Cors struct {
	handler http.Handler
	origins []string
}

func isValidOrigin(origin string, origins []string) bool {
	for _, b := range origins {
		if b == origin {
			return true
		}
	}
	return false
}

// Handle the preflight and CORS request
func (c *Cors) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	origin := strings.ToLower(r.Header.Get("Origin"))
	if r.Method == "OPTIONS" {
		fmt.Printf("Preflight request\n")
		if origin == "" || !isValidOrigin(origin, c.origins) {
			fmt.Printf("Invalid ORIGIN %s\n", origin)
			w.WriteHeader(400)
			return
		}
		// Try to avoid caches caching responses based on different versions of these
		w.Header().Add("Vary", "Origin, Access-Control-Request-Method, Access-Control-Request-Headers")
		w.Header().Set("Access-Control-Allow-Origin", origin)
		// Allow whatever is asked for
		w.Header().Set("Access-Control-Allow-Headers", r.Header.Get("Access-Control-Request-Headers"))
		w.Header().Set("Access-Control-Allow-Methods", strings.ToUpper(r.Header.Get("Access-Control-Request-Method")))
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Max-Age", "1000")
	} else {
		if origin != "" {
			fmt.Printf("Passing through CORS request\n")
			w.Header().Add("Vary", "Origin")
			w.Header().Set("Access-Control-Allow-Origin", origin)
			w.Header().Set("Access-Control-Allow-Credentials", "true")
		}
		c.handler.ServeHTTP(w, r)
	}
}

// ParseHTTPDate parses the HTTP string represention of dates, as used in
// If-Modified-Since headers. HTTP dates are only to the nearest second
// so the value to compare against will also need rounding.
func ParseHTTPDate(date string) (t time.Time, err error) {
	if date == "" {
		err = fmt.Errorf("No date found")
		return
	}
	t, err = time.Parse(http.TimeFormat, date)
	if err != nil {
		// t.Nanoseconds = 0
	}
	return
}

// CBLServer is the main server data type, it takes a PostgresSQL database as
// its input
type CBLServer struct {
	DB            *sql.DB
	BaseTemplate  *template.Template
	LoginTemplate *template.Template
	ErrorTemplate *template.Template
	PageTemplate  *template.Template
	EtagPrefix    string
	LocatorMap    map[string]string
	LoginRedirect string
}

func NewCBLServer(templateDir string, dBName string, etagPrefix string, locatorMap map[string]string, LoginRedirect string) (*CBLServer, error) {
	pdb, err := sql.Open("postgres", fmt.Sprintf("dbname=%s sslmode=disable", dBName))
	if err != nil {
		return nil, err
	}

	baseTemplate := template.Must(template.ParseFiles(filepath.Join(templateDir, "base.html")))
	return &CBLServer{
		DB:            pdb,
		BaseTemplate:  baseTemplate,
		LoginTemplate: template.Must(template.Must(baseTemplate.Clone()).ParseFiles(filepath.Join(templateDir, "login.html"))),
		PageTemplate:  template.Must(template.Must(baseTemplate.Clone()).ParseFiles(filepath.Join(templateDir, "page.html"))),
		ErrorTemplate: template.Must(template.Must(baseTemplate.Clone()).ParseFiles(filepath.Join(templateDir, "error.html"))),
		EtagPrefix:    etagPrefix,
		LocatorMap:    locatorMap,
		LoginRedirect: LoginRedirect,
	}, nil
}

// Failure is an error type that can be rendered into HTML and JSON versions.
// It is used to return anything that isn't a 200 response. Which means 304 not
// modified responses are returned via this Failure type, even though they
// aren't strictly failures.
type Failure struct {
	code     int
	Public   string `json:"error"`
	internal string
	Maybe    *Locator `json:"maybe,omitempty"`
}

// Error() returns a string representation of the error for use in messages
func (f *Failure) Error() string {
	return f.Public
}

func (f *Failure) Location() *Locator {
	if f.Maybe != nil {
		return f.Maybe
	} else {
		return nil
	}
}

// Code() returns an HTTP error code to use to represent the failure
func (f *Failure) Code() int {
	return f.code
}

// JSONError() returns a JSON representation of the failure
func (f *Failure) JSONError(correlationID uint64, revision int) []byte {
	fmt.Printf(f.Internal(correlationID, revision))
	if result, err := json.Marshal(f); err != nil {
		fmt.Printf("Error formatting the JSON for error")
		return []byte("{\"error\":\"Could not format error as JSON\"}")
	} else {
		return result
	}
}

type FailureData struct {
	*Failure
	Title string
}

func (f *Failure) HTMLError(correlationID uint64, revision int, errorTemplate *template.Template) []byte {
	fmt.Printf(f.Internal(correlationID, revision))
	response := bytes.NewBufferString("")
	if err := errorTemplate.Execute(response, FailureData{Failure: f, Title: "Error"}); err != nil {
		fmt.Printf("CRITICAL - Error occurred rendering the template: %v\n", err)
		return []byte("Error occurred rendering the template\n")
	} else {
		return response.Bytes()
	}
}

// Internal() returns a string designed for internal logging containing
// detailed information about the error. The information is not designed to be
// public-facing.
func (f *Failure) Internal(correlationID uint64, revision int) string {
	res := fmt.Sprintf("{%d} ", correlationID)
	if revision != 0 {
		res += fmt.Sprintf("[%d] ", revision)
	}
	if f.code != 0 {
		res += fmt.Sprintf("%d ", f.code)
	}
	return fmt.Sprintf("%s%s", res, f.internal)
}

// Transaction holds a DB transaction for the current request and the
// latest revision value for the database which any revisions made during the
// request will be tracked with in cbl_changes.
//
// In future it might be good just to have a request ID not tied to a revision
// and only create a new revision if a change needs to be made.
type Transaction struct {
	tx       *sql.Tx
	revision int
}

// Resource represents an abstract resource in the database. Resource gets
// embedded in other objects so that they can identify their type and internal
// ID.
type Resource struct {
	type_ string
	id    int
}

// Locator represents a Domain and Slug used to locate a resource.
type Locator struct {
	Domain string `json:"domain"`
	Slug   string `json:"slug"`
}

// Revision contains both a revision integer from the database and the
// timestamp of the latest change that occurred under that revision.
type Revision struct {
	revision int
	tstamp   time.Time
}

func (r Revision) LastModified() time.Time {
	return r.tstamp
}

func (r Revision) Etag() int {
	return r.revision
}

type Page struct {
	// *ResourceRequest
	*Revision
	*Resource
	*Locator
	pageTemplate *template.Template
	Title        string        `json:"title"`
	Content      template.HTML `json:"body"`
	body         string
	Format       string `json:"format"`
}

// Redirect holds the previous Locator and a Resource to represent a resource that was moved
type Redirect struct {
	Locator
	Resource
	revision        int
	resource_exists bool
}

// FollowLocator is used with a Transaction to find a Resource that the
// Locator points to. If no resource is found, an error compatible with the
// ErrorResponder interface must be returned.
func FollowLocator(rt *Transaction, locator *Locator) (*Resource, ErrorResponder) {
	// Look up the resource
	resource := Resource{}
	if err := rt.tx.QueryRow(`
        select type, id from cbl_locator where domain=$1 AND slug=$2
    `, locator.Domain, locator.Slug).Scan(&resource.type_, &resource.id); err != nil {
		if err.Error() == "sql: no rows in result set" {
			fmt.Printf("[%d] 404 No such cbl_locator domain: %s, slug: %s. Looking up redirects ...\n", rt.revision, locator.Domain, locator.Slug)
			if rows, err := rt.tx.Query(`
SELECT
    cbl_redirect.type
  , cbl_redirect.id
  , cbl_redirect.revision
  , cbl_redirect.resource_exists
  , cbl_locator.domain
  , cbl_locator.slug
FROM cbl_redirect
LEFT JOIN cbl_locator ON cbl_redirect.type = cbl_locator.type AND cbl_redirect.id = cbl_locator.id
WHERE
    cbl_redirect.domain = $1
AND cbl_redirect.slug = $2
ORDER BY revision DESC, redirect DESC
;
             `, locator.Domain, locator.Slug); err != nil {
				return nil, &Failure{
					code:     404,
					internal: fmt.Sprintf("ERROR Couldn't get the redirects. Error: %s\n", err),
					Public:   "Not found",
				}
			} else {
				defer rows.Close()
				var results []Redirect
				for rows.Next() {
					redirect := Redirect{}
					if err = rows.Scan(&redirect.type_, &redirect.id, &redirect.revision, &redirect.resource_exists, &redirect.Domain, &redirect.Slug); err != nil {
						return nil, &Failure{
							code:     404,
							internal: fmt.Sprintf("Couldn't get the redirect results. Probably because there aren't any and the page is not found. Error: %s\n", err),
							Public:   "Not found",
						}
					}
					results = append(results, redirect)
				}
				fmt.Printf("[%d] Redirect results: %v\n", rt.revision, results)
				// Want to find the results that still have a locator.
				if len(results) > 0 {
					return nil, &Failure{
						code:     302,
						internal: fmt.Sprintf("Got the redirect results\n"),
						Public:   "Not found",
						Maybe:    &results[0].Locator,
					}
				} else {
					return nil, &Failure{
						code:     404,
						internal: fmt.Sprintf("ERROR Couldn't get the redirect results. Error: %s\n", err),
						Public:   "Not found",
					}
				}
			}
		} else {
			return nil, &Failure{
				code:     500,
				internal: fmt.Sprintf("ERROR unexpected error looking up the URL: %s\n", err),
				Public:   "Couldn't look up the URL",
			}
		}
	}
	return &resource, nil
}

// func (h *CBLServer) ExecFile(w http.ResponseWriter, r *http.Request) (string) {
// 	// Assume html
// 	// What do we want to do here?
// 	out, err := exec.Command("node", h.RendererJSFile, r.URL.Path).Output()
// 	if err != nil {
// 		fmt.Printf("Error, renderer: %s\n", err)
// 		w.WriteHeader(500)
// 		w.Write([]byte("{\"error\":\"Couldn't get a rendered version\"\n}"))
// 		return
// 	}
// 	// w.Header().Set("Content-Length", strconv.Itoa(len(out)))
// 	w.Write(out)
// }

// GetFormat parses the Accept header and `format` query parameter to return
// either "json" or "html" to represent the format the response should be
// returned in.
func GetFormat(r *http.Request) string {
	alternatives := []string{"text/html", "application/json", "text/json"}
	accept := r.Header.Get("Accept")
	if accept == "" {
		accept = "text/html"
	}
	content_type := goautoneg.Negotiate(r.Header.Get("Accept"), alternatives)
	format := "html"
	if content_type == "application/json" {
		format = "json"
	}
	q := r.URL.Query()
	if q.Get("format") == "json" {
		format = "json"
	}
	return format
}

// ErrorResponder is an interface that represents an error that can be
// formatted and returned to the user as an HTTP response.
type ErrorResponder interface {
	Code() int
	Internal(correlationID uint64, revision int) string
	Error() string
	Location() *Locator
	JSONError(correlationID uint64, revision int) []byte
	HTMLError(correlationID uint64, revision int, errorTemplate *template.Template) []byte
}

// SuccessResponder represents a successful resource retrieval from the DB,
// possibly with lots of child resources to. It can be used to create an HTTP
// response.
type SuccessResponder interface {
	LastModified() time.Time
	Etag() int
	JSON() ([]byte, ErrorResponder)
	HTML() ([]byte, ErrorResponder)
}

func StartTransaction(DB *sql.DB, locator *Locator) (rt *Transaction, fail ErrorResponder) {
	tx, err := DB.Begin()
	if err != nil {
		return nil, &Failure{
			code:     500,
			Public:   "Couldn't start DB transaction",
			internal: fmt.Sprintf("Couldn't begin a transaction visiting domain %s, slug %s, is the database server running? Error: %s\n", locator.Domain, locator.Slug, err),
		}
	}
	rt = &Transaction{tx: tx}
	if err = tx.QueryRow(`select nextval('cbl_revision_id');`).Scan(&rt.revision); err != nil {
		return nil, &Failure{
			code:     500,
			Public:   "Couldn't get a new revision ID",
			internal: fmt.Sprintf("Couldn't get a new revision ID visiting domain %s, slug %s. Error: %s\n", locator.Domain, locator.Slug, err),
		}
	}
	return rt, nil
}

func NewRevision() *Revision {
	return &Revision{tstamp: time.Time{}, revision: 0}
}

func HandlePage(request ResourceRequester) (SuccessResponder, ErrorResponder) {
	page := &Page{pageTemplate: request.PageTemplate(), Revision: request.ResourceLastRevised()}
	if err := request.Transaction().tx.QueryRow(`
SELECT
    page.title
  , page.format
  , page.body
FROM page
WHERE page.id=$1
LIMIT 1
     ;
     `, request.Resource().id).Scan(&page.Title, &page.Format, &page.body); err != nil {
		return nil, &Failure{
			code:     500,
			internal: fmt.Sprintf("Unknown error getting page %d: %s\n", request.Resource().id, err),
			Public:   "Couldn't get the page",
		}
	}
	return page, nil
}

func (p Page) JSON() ([]byte, ErrorResponder) {
	if p.Content == "" {
		if p.Format == "markdown" {
			unsafe := blackfriday.MarkdownCommon([]byte(p.body))
			p.Content = template.HTML(bluemonday.UGCPolicy().SanitizeBytes(unsafe))
		} else {
			p.Content = template.HTML(bluemonday.UGCPolicy().SanitizeBytes([]byte(p.body)))
		}
	}
	if result, err := json.Marshal(p); err != nil {
		return nil, &Failure{
			code:     500,
			internal: fmt.Sprintf("Couldn't encode the page JSON: %s\n", err),
			Public:   "Couldn't encode the page JSON",
		}
	} else {
		return result, nil
	}
}

func (p Page) HTML() ([]byte, ErrorResponder) {
	response := bytes.NewBufferString("")
	if p.Content == "" {
		if p.Format == "markdown" {
			unsafe := blackfriday.MarkdownCommon([]byte(p.body))
			p.Content = template.HTML(bluemonday.UGCPolicy().SanitizeBytes(unsafe))
		} else {
			p.Content = template.HTML(bluemonday.UGCPolicy().SanitizeBytes([]byte(p.body)))
		}
	}
	if err := p.pageTemplate.Execute(response, p); err != nil {
		return nil, &Failure{
			code:     500,
			Public:   "Could not render the HTML template",
			internal: fmt.Sprintf("Couldn't render te HTML login template with data %v. Error: %s\n", p, err),
		}
	} else {
		return response.Bytes(), nil // XXX Should use the writer interface really.
	}
}

type Login struct {
	*Revision
	Title         string
	Body          string
	LoginTemplate *template.Template
}

func (p *Login) JSON() ([]byte, ErrorResponder) {
	if result, err := json.Marshal(p); err != nil {
		return nil, &Failure{
			code:     500,
			internal: fmt.Sprintf("Couldn't encode the page JSON: %s\n", err),
			Public:   "Couldn't encode the page JSON",
		}
	} else {
		return result, nil
	}
}

func (p *Login) HTML() ([]byte, ErrorResponder) {
	response := bytes.NewBufferString("")
	if err := p.LoginTemplate.Execute(response, p); err != nil {
		return nil, &Failure{
			code:     500,
			Public:   "Could not render the HTML template",
			internal: fmt.Sprintf("Couldn't render te HTML login template with data %v. Error: %s\n", p, err),
		}
	} else {
		return response.Bytes(), nil // XXX Should use the writer interface really.
	}
}

func (h *CBLServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("REQUEST: %s\n", r.URL.Path)
	request := &Request{correlationID: atomic.AddUint64(&correlationID, 1), pageTemplate: h.PageTemplate, errorTemplate: h.ErrorTemplate, locator: &Locator{Domain: strings.Split(r.Host, ":")[0], Slug: r.URL.Path}}
	var successResponder SuccessResponder
	var errorResponder ErrorResponder
	if rt, err := StartTransaction(h.DB, request.Locator()); err != nil {
		errorResponder = err
	} else {
		defer rt.tx.Rollback()
		dbrequest := &DBRequest{rt: rt, Request: request}
		if dbrequest.Locator().Slug == "/login" {
			if r.Method == "GET" {
				successResponder = &Login{
					Revision:      NewRevision(),
					Title:         "Login",
					Body:          "",
					LoginTemplate: h.LoginTemplate,
				}
			} else {
				if session, _, err := CheckPassword(dbrequest.Transaction(), r.RemoteAddr, r.FormValue("username"), r.FormValue("password")); err != nil {
					errorResponder = err
				} else {
					expiration := time.Now().Add(1 * time.Hour)
					sessionCookie := &http.Cookie{Name: "session", Value: session, HttpOnly: false, Expires: expiration}
					http.SetCookie(w, sessionCookie)
					// XXX Might as well do this properly at some point?
					successResponder = &Page{pageTemplate: request.PageTemplate(), Title: "Success", Content: template.HTML([]byte("Logged in successfully. <a href=\"" + h.LoginRedirect + "\">Continue</a>")), Revision: NewRevision()}
				}
			}
		} else {
			dbrequest.SetIfModifiedSince(r.Header.Get("If-Modified-Since"))
			dbrequest.SetIfNoneMatch(r.Header.Get("If-None-Match"))
			dbrequest.SetFormat(GetFormat(r))

			sessionStr := ""
			for _, k := range r.Cookies() {
				if k.Name == "session" {
					sessionStr = k.Value
					break
				}
			}

			if sessionStr != "" {
				if session, err := GetSession(rt, sessionStr); err != nil {
					errorResponder = err
					dbrequest.shouldLogoutCookie = true
				} else {
					dbrequest.SetSession(session)
					successResponder, errorResponder = h.HandleResource(&ResourceRequest{DBRequest: dbrequest})
				}
			} else {
				successResponder, errorResponder = h.HandleResource(&ResourceRequest{DBRequest: dbrequest})
			}
		}
		var b []byte
		if dbrequest.ShouldLogoutCookie() {
			sessionCookie := &http.Cookie{Name: "session", Value: "", HttpOnly: false, Expires: time.Now(), MaxAge: -1}
			http.SetCookie(w, sessionCookie)
		}
		dbrequest.Transaction().tx.Commit()
		if dbrequest.Format() == "json" {
			w.Header().Set("Content-Type", "application/json")
		} else {
			w.Header().Set("Content-Type", "text/html")
		}
		w.Header().Add("Vary", "Accept")
		if errorResponder == nil {
			if dbrequest.Format() == "json" {
				b, errorResponder = successResponder.JSON()
			} else {
				b, errorResponder = successResponder.HTML()
			}
		}
		if errorResponder != nil {
			location := errorResponder.Location()
			if location != nil {
				w.Header().Set("Location", h.LocatorMap[location.Domain]+location.Slug)
			}
			w.WriteHeader(errorResponder.Code())
			if dbrequest.Format() == "json" {
				w.Write(errorResponder.JSONError(dbrequest.correlationID, dbrequest.Transaction().revision))
			} else {
				w.Write(errorResponder.HTMLError(dbrequest.correlationID, dbrequest.Transaction().revision, h.ErrorTemplate))
			}
		} else {
			if !successResponder.LastModified().IsZero() {
				w.Header().Set("Last-Modified", successResponder.LastModified().Format(http.TimeFormat))
			}
			if successResponder.Etag() != 0 {
				w.Header().Set("Etag", fmt.Sprintf("%s%d", h.EtagPrefix, successResponder.Etag()))
			}
			w.Write(b)
		}
	}
}

func CheckPassword(rt *Transaction, ip string, username string, password string) (string, string, ErrorResponder) {
	var result bool
	if err := rt.tx.QueryRow(`
SELECT true FROM pg_shadow WHERE usename=$1 AND passwd='md5' || md5($2 || $3);
`, username, username, password).Scan(&result); err != nil {
		return "", "", &Failure{
			code:     401,
			Public:   "Could not sign in with those credentials",
			internal: fmt.Sprintf("Couldn't login with username: %s and password %s. Error: %s\n", username, password, err),
		}
	}
	var loginAttempt int
	if err := rt.tx.QueryRow("INSERT into cbl_login_attempt (username, ip) values ($1, $2) RETURNING id", username, ip).Scan(&loginAttempt); err != nil {
		return "", "", &Failure{
			code:     401,
			Public:   "Could not log the sign in attempt",
			internal: fmt.Sprintf("Couldn't log the sign in attempt with username: %s and password %s. Error: %s\n", username, password, err),
		}
	}
	var csrf string
	var session string
	if err := rt.tx.QueryRow("INSERT into session (login_attempt) values ($1) RETURNING session, csrf", loginAttempt).Scan(&session, &csrf); err != nil {
		fmt.Printf("%s\n", err)
		return "", "", &Failure{
			code:     401,
			Public:   "Could not create a new session",
			internal: fmt.Sprintf("Couldn't create a new session for login attempt %d with username: %s and password %s. Error: %s\n", loginAttempt, username, password, err),
		}
	}
	return session, csrf, nil
}

type Session struct {
	session  string
	csrf     string
	username string
	at       time.Time
}

func LogoutSession(rt *Transaction, session string) ErrorResponder {
	var logout time.Time
	if err := rt.tx.QueryRow(`
    UPDATE session SET logout=NOW() WHERE session=$1 AND logout IS NULL RETURNING logout;
`, session).Scan(&logout); err != nil {
		fmt.Printf("%s\n", err)
		return &Failure{
			code:     401,
			Public:   "Could not log out session",
			internal: fmt.Sprintf("Couldn't log out session: %s. Error: %s\n", session, err),
		}
	}
	return nil
}

func GetSession(rt *Transaction, session string) (*Session, ErrorResponder) {
	var result Session
	if err := rt.tx.QueryRow(`
    SELECT
        session
      , csrf
      , username
      , at
    FROM session
    LEFT JOIN cbl_login_attempt on session.login_attempt = cbl_login_attempt.id
    WHERE session = $1
    AND at + interval '600 seconds' > NOW()
    ;
`, session).Scan(&result.session, &result.csrf, &result.username, &result.at); err != nil {
		return nil, &Failure{
			code:     401,
			Public:   "Could not get session",
			internal: fmt.Sprintf("Couldn't get session: %s. Error: %s\n", session, err),
		}
	}
	return &result, nil
}

func GetResourceLastRevised(etagPrefix string, rt *Transaction, resource *Resource, ims string, inm string) (*Revision, ErrorResponder) {
	resourceLastRevised := &Revision{}
	if err := rt.tx.QueryRow(`
SELECT
    cbl_change.revision
  , cbl_change.tstamp
FROM cbl_change
WHERE id=$1 and type=$2
ORDER BY cbl_change.tstamp DESC
LIMIT 1
     ;
     `, resource.id, resource.type_).Scan(&resourceLastRevised.revision, &resourceLastRevised.tstamp); err != nil {
		return nil, &Failure{
			code:     500,
			internal: fmt.Sprintf("Unknown error getting resource revision for %s %d: %s\n", resource.type_, resource.id, err),
			Public:   "Couldn't get the page",
		}
	}
	if etagPrefix+strconv.Itoa(resourceLastRevised.revision) == inm {
		return nil, &Failure{
			code:     304,
			Public:   "Not modified", // XXX Shouldn't really treat this is an error
			internal: fmt.Sprintf("Not modified due to etag\n"),
		}
	}
	// Only use IfModified Since, if not Etag is sent
	if inm == "" {
		var ifModifiedSince time.Time
		if ims != "" {
			var err error
			ifModifiedSince, err = ParseHTTPDate(ims)
			if err != nil {
				return nil, &Failure{
					code:     400,
					Public:   "Bad If-Modified-Since header",
					internal: fmt.Sprintf("ERROR Couldn't parse the If-Modified-Since header, returning bad request. %s\n", err),
				}
			}
		}
		// Find out if this revision is still valid - but we need to locate things first
		if !ifModifiedSince.IsZero() && resourceLastRevised.tstamp.Format(http.TimeFormat) == ifModifiedSince.Format(http.TimeFormat) {
			return nil, &Failure{
				code:     304,
				Public:   "Not modified", // XXX Shouldn't really treat this is an error
				internal: fmt.Sprintf("Not modified since Last-Modified\n"),
			}
		}
	}
	return resourceLastRevised, nil
}

// type Responder interface {
//     Success() (bool)
//     SetSuccessResponder(SuccessResponder)
//     SuccessResponder() (SuccessResponder)
//     SetErrorResponder(ErrorResponder)
//     ErrorResponder() (ErrorResponder)
// }

type Requester interface {
	SetLocator(*Locator)
	Locator() *Locator
	PageTemplate() *template.Template
}

type DBRequester interface {
	Requester
	SetTransaction(*Transaction)
	Transaction() *Transaction
	Logout() ErrorResponder
	ShouldLogoutCookie() bool
	SetSession(*Session)
	Session() *Session
}

type ResourceRequester interface {
	DBRequester
	SetFormat(string)
	Format() string
	SetIfModifiedSince(string)
	IfModifiedSince() string
	SetIfNoneMatch(string)
	IfNoneMatch() string
	SetResource(*Resource)
	Resource() *Resource
	// These two are really shared between request and response. The are used
	// on the request side to quickly return a 304 where possible, and on the
	// response side for setting Last-Modified and Etag headers.
	SetResourceLastRevised(resourceLastRevised *Revision)
	ResourceLastRevised() *Revision
}

type Request struct {
	correlationID uint64
	locator       *Locator
	format        string
	ims           string
	inm           string
	pageTemplate  *template.Template
	errorTemplate *template.Template
}

func (r *Request) SetLocator(locator *Locator) {
	r.locator = locator
}
func (r *Request) Locator() *Locator {
	return r.locator
}
func (r *Request) PageTemplate() *template.Template {
	return r.pageTemplate
}
func (r *Request) SetFormat(format string) {
	r.format = format
}
func (r *Request) Format() string {
	return r.format
}
func (r *Request) SetIfModifiedSince(ims string) {
	r.ims = ims
}
func (r *Request) IfModifiedSince() string {
	return r.ims
}
func (r *Request) SetIfNoneMatch(inm string) {
	r.inm = inm
}
func (r *Request) IfNoneMatch() string {
	return r.inm
}

type DBRequest struct {
	*Request
	rt                 *Transaction
	shouldLogoutCookie bool
	session            *Session
}

func (dbr *DBRequest) SetTransaction(rt *Transaction) {
	dbr.rt = rt
}
func (dbr *DBRequest) Logout() ErrorResponder {
	dbr.shouldLogoutCookie = true
	return LogoutSession(dbr.rt, dbr.session.session)
}
func (dbr *DBRequest) ShouldLogoutCookie() bool {
	return dbr.shouldLogoutCookie
}
func (dbr *DBRequest) Transaction() *Transaction {
	return dbr.rt
}
func (dbr *DBRequest) SetSession(session *Session) {
	dbr.session = session
}
func (dbr *DBRequest) Session() *Session {
	return dbr.session
}

type ResourceRequest struct {
	*DBRequest
	resource            *Resource
	resourceLastRevised *Revision
}

func (rr *ResourceRequest) SetResource(resource *Resource) {
	rr.resource = resource
}
func (rr *ResourceRequest) Resource() *Resource {
	return rr.resource
}
func (rr *ResourceRequest) SetResourceLastRevised(resourceLastRevised *Revision) {
	rr.resourceLastRevised = resourceLastRevised
}
func (rr *ResourceRequest) ResourceLastRevised() *Revision {
	return rr.resourceLastRevised
}

func (h *CBLServer) HandleResource(request ResourceRequester) (SuccessResponder, ErrorResponder) {
	if request.Locator().Slug == "/logout" {
		if request.Session() == nil {
			return nil, &Failure{
				code:     401,
				Public:   "Not signed in",
				internal: fmt.Sprintf("Not signed in\n"),
			}
		} else {
			if err := request.Logout(); err != nil {
				return nil, err
			}
			return &Page{pageTemplate: request.PageTemplate(), Title: "Signed out", body: "", Revision: NewRevision()}, nil
		}
	}
	if resource, err := FollowLocator(request.Transaction(), request.Locator()); err != nil {
		return nil, err
	} else {
		request.SetResource(resource)
		if resourceLastRevised, err := GetResourceLastRevised(h.EtagPrefix, request.Transaction(), request.Resource(), request.IfModifiedSince(), request.IfNoneMatch()); err != nil {
			return nil, err
		} else {
			request.SetResourceLastRevised(resourceLastRevised)
			// fmt.Printf("[%d] Request format: %s, domain: %s, slug: %s, for %s %d, revision: %d, %s\n", rt.revision, format, locator.Domain, locator.Slug, resource.type_, resource.id, resourceLastRevised.revision, resourceLastRevised.tstamp.Format(http.TimeFormat))
			switch resource.type_ {
			case "page":
				return HandlePage(request)
			}
			return nil, &Failure{
				code:     500,
				Public:   "Unknown type",
				internal: fmt.Sprintf("Unknown type: %s\n", resource.type_),
			}
		}
	}
}
