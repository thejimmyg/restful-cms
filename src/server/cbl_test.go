package cbl

import (
	"cbl"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestLoginLogout(t *testing.T) {
	locatorMap := map[string]string{
		"localhost": "http://localhost:8071",
	}
	cbls, err := cbl.NewCBLServer("tmpl", "james2", "", locatorMap, "")
	if err != nil {
		log.Fatal("Couldn't create server. Error: ", err)
	}
	defer cbls.DB.Close()
	ts := httptest.NewServer(cbls)
	defer ts.Close()

	// Login Form
	res, err := http.Get(ts.URL + "/login")
	if err != nil {
		t.Fatal(err)
	}
	rawbody, err := ioutil.ReadAll(res.Body)
	body := string(rawbody)
	res.Body.Close()
	if err != nil {
		t.Fatal(err)
	}
	expected := `<input type="text"     id="username" name="username" value="">`
	if !strings.Contains(body, expected) {
		t.Fatal("No username field: %s %s", expected, body)
	}
	expected = `<input type="password" id="password" name="password" value="">`
	if !strings.Contains(body, expected) {
		t.Fatal("No username field: %s %s", expected, body)
	}
}
