package main

import (
	"cbl"
	"flag"
	"fmt"
	"github.com/NYTimes/gziphandler"
	_ "github.com/lib/pq"
	"github.com/thejimmyg/iniflags"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	certFile := flag.String("certFile", "", "path to the full certificate chain .pem file")
	keyFile := flag.String("keyFile", "", "path to the private key .pem file")

	gZip := flag.Bool("gZip", false, "enable GZip compression if the client requests it")
	corsOrigins := flag.String("corsOrigins", "", "origins to allow for CORS")
	etagPrefix := flag.String("etagPrefix", "", "change the etag prefix, e.g. if you have changed a base template and want to invalidate old cached pages")

	port := flag.Int("port", 443, "port to serve on")
	host := flag.String("host", "localhost", "port to serve on")

	cssFile := flag.String("cssFile", "", "path to the CSS file to serve at /app.css")
	jsFile := flag.String("jsFile", "", "path to the JS file to serve at /app.js")
	templateDir := flag.String("templateDir", "", "path to the templates directory")
	staticDir := flag.String("staticDir", "", "directory to serve application assets at /static from")
	dbName := flag.String("dbName", "", "postgresql database name for the wiki")
	// rendererJsFile := flag.String("rendererJsFile", "", "path to the JS file to execute for the render")
	iniflags.Parse() // instead of flag.Parse()
	if *cssFile == "" || *jsFile == "" || *staticDir == "" || *templateDir == "" {
		fmt.Printf("Please specify -cssFile, -jsFile, -templateDir and -staticDir\n")
		os.Exit(1)
	}
	if _, err := os.Stat(*templateDir); os.IsNotExist(err) {
		fmt.Printf("No such template directory: %s\n", *templateDir)
		os.Exit(1)
	}
	if _, err := os.Stat(*jsFile); os.IsNotExist(err) {
		fmt.Printf("No such JS file %s\n", *jsFile)
		os.Exit(1)
	}
	if _, err := os.Stat(*cssFile); os.IsNotExist(err) {
		fmt.Printf("No such CSS file %s\n", *cssFile)
		os.Exit(1)
	}
	if _, err := os.Stat(*staticDir); os.IsNotExist(err) {
		fmt.Printf("No such directory for -staticDir: %s\n", *staticDir)
		os.Exit(1)
	}
	if *dbName == "" {
		fmt.Printf("Please specify -dbName\n")
		os.Exit(1)
	}
	sm := http.NewServeMux()
	log.Printf("About to listen on %s:%d\n", *host, *port)
	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", *host, *port),
		Handler: sm,
	}
	if *etagPrefix != "" {
		log.Printf("Using Etag prefix \"%s\"\n", *etagPrefix)
	}
	locatorMap := map[string]string{
		"jimmyg":    "http://localhost:8071",
		"localhost": "http://localhost:8071",
	}
	log.Printf("Locator map: %v\n", locatorMap)
	cbls, err := cbl.NewCBLServer(*templateDir, *dbName, *etagPrefix, locatorMap, "/e")
	if err != nil {
		log.Fatal("Couldn't create server. Error: ", err)
	}
	defer cbls.DB.Close()

	var handler http.Handler
	handler = cbls
	if *gZip == true {
		log.Printf("Using GZip support\n")
		handler = gziphandler.GzipHandler(handler)
	}
	if *corsOrigins != "" {
		log.Printf("Using CORS support\n")
		handler = cbl.NewCors(handler, strings.Split(strings.ToLower(strings.Replace(*corsOrigins, " ", "", -1)), ","))
	}
	sm.Handle("/", handler)
	sm.HandleFunc("/app.css", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, *cssFile)
	})
	sm.HandleFunc("/app.js", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, *jsFile)
	})
	sm.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir(*staticDir))))
	if *certFile != "" && *keyFile != "" {
		log.Printf("Using TLS\n")
		log.Printf("Go to https://%s:%d/\n", *host, *port)
		log.Fatal(srv.ListenAndServeTLS(*certFile, *keyFile))
	} else {
		log.Printf("Using plain HTTP, with no TLS\n")
		log.Printf("Go to http://%s:%d/\n", *host, *port)
		log.Fatal(srv.ListenAndServe())
	}
}
